<?php

/**
 * Full list of films from 'film_list' view
 */
const REQ_MSG_FULL_LIST = "SELECT * FROM t_msg";

/**
 * Informations about 1 film
 *
 * @var :ValFilmID integer between 1 and 99999 - MANDATORY - NO DEFAULT
 */
const REQ_MSG_INFO = "SELECT *
FROM t_msg
INNER JOIN t_email ON t_msg.id_msg = t_email.id_email";

/**
 * Add a film
 *
 * @var :ValTitle string max length 255 - MANDATORY - NO DEFAULT
 * @var :ValDescription string max length 65535 - OPTIONAL
 * @var :ValReleaseYear year 4 digits - OPTIONAL
 * @var :ValLanguageID integer as from 'language' table, between 1 and 999 - MANDATORY - NO DEFAULT
 */
const REQ_FILM_ADD = "INSERT INTO film (title, description, release_year, language_id) VALUES (:ValTitle, :ValDescription, :ValReleaseYear, :ValLanguageID)";

/**
 * Modify film title
 *
 * @var :ValNewTitle string max length 255 - MANDATORY
 * @var :ValFilmID integer between 1 and 99999 - MANDATORY - NO DEFAULT
 */
const REQ_MSG_UNREAD = "SELECT * FROM t_msg WHERE etat = 'À traiter' ";

/**
 * Delete a film
 *
 * @var :ValFilmID integer between 1 and 99999 - MANDATORY - NO DEFAULT
 */
const REQ_FILM_DELETE = "DELETE FROM film WHERE film_id = :ValFilmID";

/**
 * Add a film
 *
 * @var :ValTitle string max length 255 - MANDATORY - NO DEFAULT
 * @var :ValDescription string max length 65535 - OPTIONAL
 * @var :ValReleaseYear year 4 digits - OPTIONAL
 * @var :ValLanguageID integer as from 'language' table, between 1 and 999 - MANDATORY - NO DEFAULT
 */
// const REQ_FILM_ADD_COMPLETE = "INSERT INTO film (title, description, release_year, language_id, original_language_id, rental_duration, rental_rate, length, replacement_cost, rating, special_features) VALUES (:ValTitle, :ValDescription, :ValReleaseYear, :ValLanguageID, :ValOriginalLanguageID, :ValRentalDuration, :ValRentalRate, :ValLength, :ValReplacementCost, :ValRating, :ValSpecialFeatures)";
const REQ_FILM_ADD_COMPLETE = "INSERT INTO film (title, description, release_year, language_id, original_language_id, rental_duration, rental_rate, length, replacement_cost, rating, special_features) VALUES (:title, :description, :release_year, :language_id, :original_language_id, :rental_duration, :rental_rate, :length, :replacement_cost, :rating, :special_features)";

//Ajouter des films à la table Favoris

//Get all msg
//Contact msg
//
const AddEmail ="INSERT INTO t_email (Email) VALUES (:valEmail)";
const GetEmail = "set @mon_email = (SELECT id_Email FROM t_email WHERE Email = :valEmail)";
const AddPerson = "INSERT INTO t_personne (prenom,nom,tel,id_email_personne) VALUES (:valPrenom,:valNom,:valTel,@mon_email)";



const HowManyByMonth = "SELECT * FROM t_msg WHERE month(date(date_msg)) = :thismonth ";